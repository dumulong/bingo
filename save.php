<?php

header("Content-Type: text/html; charset=UTF-8");
header("Cache-Control: no-cache, must-revalidate"); 
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//error handler function
function customError($errno, $errstr) {
    http_response_code(400);
    header("Content-Type: application/json");
    echo "{ \"error\": { \"message\": \"{$errstr}\", \"code\": \"{$errno}\" }}";
    exit();
}

//set error handler
set_error_handler("customError");

file_put_contents('words.json', file_get_contents("php://input"));

echo(file_get_contents("php://input"));

?>