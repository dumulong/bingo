
class Board {

  constructor (size = 5) {
    this.size = size;
    this.centerNdx = (((size * size) - 1) / 2 ) + 1; // Position of the center square
    this.winPatterns = this.buildWinPatterns(); // Generate a list of winning patterns

    this.squareSelected = new Array(size * size).fill(0); // Fill new array with 0 (unselected squares)
    this.squareSelected[this.centerNdx-1] = 1; // Set the center "FREE" as selected

    this.hasBingo = false;
  }

  buildWinPatterns () {
    const patterns = [];
    const winDown = [];
    const winUp = [];
    for (let i=0; i < this.size; i++) {
      let winH = [];
      let winV = [];
      for (let j=0; j < this.size; j++) {
        winH.push(j + (i*this.size)); // Horizontal
        winV.push(i + (j*this.size)); // Vertical
      }
      patterns.push(winH);
      patterns.push(winV);
      winDown.push((i*this.size) + i); // Diagonal down
      winUp.push(((i+1)*this.size) - (i + 1)); // Diagonal up
    }
    patterns.push(winDown);
    patterns.push(winUp);
    return patterns;
  }

}

class boardUtils {

  static getWinPatterns (board) {
    return board.winPatterns.filter (winPattern => {
      return winPattern.reduce ((found, cell) => {
        return (found && (board.squareSelected[cell] === 1));
      }, true);
    });
  }

  static getShuffledPos (board) {

    const shuffleArray = (array) => {
      let currentIndex = array.length;
      // While there remain elements to shuffle...
      while (0 !== currentIndex) {
          // Pick a remaining element...
          const randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          // And swap it with the current element.
          const temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
      }
      return array;
    }

    let shuffledPos = [...Array(board.size*board.size).keys()]; // generate [0, 1, 2, etc]
    shuffledPos.splice(board.centerNdx,1); // Remove the center
    shuffledPos = shuffleArray(shuffledPos); // Shuffle (base on the current seed)
    shuffledPos.splice(board.centerNdx-1, 0, board.centerNdx); // Add the center
    return shuffledPos;
  }

}

function buildBoard () {

  const boardDiv = document.querySelector(".board");
  const rowTemplate = document.querySelector("#rowTemplate").innerHTML;
  const squareTemplate = document.querySelector("#squareTemplate").innerHTML;

  const words = getWords ();
  const shuffledPos = boardUtils.getShuffledPos(bingoBoard);

  const boardData = [];
  for (var row=0; row < bingoBoard.size; row++) {
    const boardRows = [];
    for (var col=0; col < bingoBoard.size; col++) {
        const squareId = (col + (row*bingoBoard.size));
        const squareObj = {
            squareId,
            word: words[shuffledPos[squareId]],
            classes : (squareId == bingoBoard.centerNdx-1 ? " center selected" : "")
        }
        const squareHTML = squareTemplate.supplant(squareObj);
        boardRows.push(squareHTML);
    }
    const boardRowsHTML = rowTemplate.supplant ({row: boardRows.join("")});
    boardData.push(boardRowsHTML);
  };
  boardDiv.innerHTML = boardData.join("");
  addBoardClicks();
}

function shuffleBoard () {
	var ans = confirm("Are you sure you want to clear the board?");
	if (ans == true) {
    setRandomSeed(true);
    location.reload();
	}
}

function addBoardClicks () {

  const squares = document.querySelectorAll(".board-col");

  squares.forEach(square => {
    square.addEventListener("click", (event) => {
      const squareSel = event.target;
      const squareId = squareSel.dataset["squareId"];
      if (!squareSel.classList.contains('center')) {
        // Select/unselect the current square
        bingoBoard.squareSelected[squareId] = (squareSel.classList.contains('selected') ? 0 : 1)
        squareSel.classList.toggle('selected');
        // Show the winning patterns
        clearWinningPatterns();
        const wins = boardUtils.getWinPatterns(bingoBoard);
        if (wins.length) {
          highlightWinPatterns(wins);
          if (!bingoBoard.hasBingo) {
            const winMsg = document.querySelector(".win-message");
            winMsg.classList.add("show-message")
            setTimeout(() => {winMsg.classList.remove("show-message")}, 800);
          }
        }
        bingoBoard.hasBingo = wins.length > 0
      }
    })
  })
}

function highlightWinPatterns (patterns) {
  patterns.forEach(pattern => {
    pattern.forEach(square => {
      const div = document.querySelector(`[data-square-id="${square}"]`);
      div.classList.add("winning-pattern");
    })
  })
}

function clearWinningPatterns () {
  const divList = document.querySelectorAll(".winning-pattern");
  for (const div of divList) {
    div.classList.remove("winning-pattern");
  }
}

// By setting the Random seed, we ensure that the board will always
// be the same when we refresh the page but will change if we click
// on Shuffle/Clear the board.
function setRandomSeed (reset = false) {
  let seed = localStorage.getItem('board_seed');
  if (!seed || reset) {
      seed = Math.random();
      localStorage.setItem('board_seed', seed);
  }
  Math.seedrandom(seed);
}

function getWords () {
  const resp = $.ajax({
    url: "words.json",
    async: false,
    cache: false,
    dataType: "json"
  });
  const words = resp.responseJSON.words;
  words.splice(bingoBoard.centerNdx, 0, "FREE"); // Add the center "FREE"
  return words;
}

// Helper function: given an object o, replace {var} with value of property o.var for any string
String.prototype.supplant = function (o) {
  return this.replace(
      /{([^{}]*)}/g,
      function (a, b) {
          var r = o[b];
          return typeof r === 'string' || typeof r === 'number' ? r : a;
      }
  );
};