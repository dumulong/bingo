# Bingo


# Don't forget to set the permissions for the list of words!
```
chmod 766 words.json
```

# Sample: words.json
```
{
  "words": [
    "Impediment",
    "Story",
    "Scrum",
    "USAA",
    "Parking lot",
    "QTest",
    "Merge request",
    "Merlin",
    "Innovation",
    "Sprint",
    "Be a Compliant Company (BCC)",
    "Bugs",
    "Return To Work (RTO)",
    "LMS",
    "Slack Channel",
    "Emails",
    "Point of contact (POC)",
    "Demo",
    "QWC",
    "PAAL",
    "Time Entry - EPMS",
    "CIT box",
    "Rumors",
    "Runway"
  ]
}
```